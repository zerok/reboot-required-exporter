module gitlab.com/zerok/reboot-required-exporter

go 1.15

require (
	github.com/prometheus/client_golang v1.9.0
	github.com/rs/zerolog v1.20.0
	github.com/spf13/afero v1.5.1
	github.com/spf13/pflag v1.0.1
	github.com/stretchr/testify v1.4.0
)
