all: bin/reboot-required-exporter

bin:
	mkdir -p bin

bin/reboot-required-exporter: $(shell find . -name '*.go') go.mod bin
	go build -o bin/reboot-required-exporter

clean:
	rm -rf bin

test:
	go test ./... -v

.PHONY: clean all test
