package main

import (
	"context"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"os"
	"time"

	"github.com/rs/zerolog"
	"github.com/spf13/pflag"
	"gitlab.com/zerok/reboot-required-exporter/internal/detector"
	"gitlab.com/zerok/reboot-required-exporter/internal/monitor"
)

var commit string
var version string
var date string

func main() {
	logger := zerolog.New(zerolog.ConsoleWriter{Out: os.Stdout}).With().Timestamp().Logger().Level(zerolog.InfoLevel)
	var oneShot bool
	var debug bool
	var httpAddr string
	var sampleTimeout time.Duration
	var sampleInterval time.Duration
	var showVersion bool
	pflag.BoolVar(&oneShot, "one-shot", false, "Do a single sample and write to stdout")
	pflag.StringVar(&httpAddr, "http-addr", "", "Address to listen on for HTTP requests")
	pflag.BoolVar(&debug, "debug", false, "Set log-level to debug")
	pflag.DurationVar(&sampleInterval, "sample-interval", time.Second*10, "How often is the check run")
	pflag.DurationVar(&sampleTimeout, "sample-timeout", time.Second*1, "How long can a check take")
	pflag.BoolVar(&showVersion, "version", false, "Show verion information")
	pflag.Parse()

	if showVersion {
		fmt.Printf("Version:  %s\nCommit:   %s\nBuilt at: %s\n", version, commit, date)
		os.Exit(0)
	}

	if debug {
		logger = logger.Level(zerolog.DebugLevel)
	}
	ctx := logger.WithContext(context.Background())

	if sampleInterval <= sampleTimeout {
		logger.Fatal().Msg("The sample-interval must be longer than the sample-timeout.")
	}
	if sampleInterval < time.Second {
		logger.Fatal().Msg("The sample-interval must be longer than a second.")
	}

	mon := monitor.NewMonitor(monitor.WithDetector(detector.NewDebianDetector()))
	if oneShot {
		if err := mon.Sample(ctx); err != nil {
			logger.Fatal().Err(err).Msg("Failed to fetch sample.")
		}
		r := httptest.NewRequest(http.MethodGet, "/", nil)
		w := httptest.NewRecorder()
		mon.ServeHTTP(w, r)
		if _, err := io.Copy(os.Stdout, w.Body); err != nil {
			logger.Fatal().Err(err).Msg("Failed to output data.")
		}
		return
	}
	if httpAddr != "" {
		ctx, cancel := context.WithCancel(ctx)
		defer cancel()
		go func() {
			tctx, cancel := context.WithTimeout(ctx, sampleTimeout)
			if err := mon.Sample(tctx); err != nil {
				logger.Error().Err(err).Msg("Sampling failed.")
			}
			cancel()
			ticker := time.NewTicker(sampleInterval)
			for {
				select {
				case <-ctx.Done():
					return
				case <-ticker.C:
					tctx, cancel := context.WithTimeout(ctx, sampleTimeout)
					if err := mon.Sample(tctx); err != nil {
						logger.Error().Err(err).Msg("Sampling failed.")
					}
					cancel()
				}
			}
		}()
		srv := http.Server{}
		srv.Handler = mon
		srv.Addr = httpAddr
		logger.Info().Msgf("Starting server on %s...", httpAddr)
		if err := srv.ListenAndServe(); err != nil {
			logger.Fatal().Err(err).Msg("Server failed.")
		}
		return
	}
	logger.Fatal().Msg("Please specify either --http-addr or --one-shot.")
}
