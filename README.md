# reboot-required-exporter

reboot-required-exporter is a small exporter for Prometheus that exposes a
single metric:

```
# HELP reboot_required Does the system require a reboot
# TYPE reboot_required gauge
reboot_required 0
```

Basically, if the operating system hints that a system reboot is necessary for
the latest updated to also be used, this metric will be set to 1. Otherwise it
will stay at 0.

For now, only Debian based systems are supported as they expose that
information through the existance of a single file: `/var/run/reboot-required`.

The exporter supports two modes of operation:

- Running as a full exporter
- Running as a one-shot command that can write to node_exporter's textfile.d
  folder
 
## Server mode

```
$ reboot-required \
    --http-addr localhost:9090
```

## One-shot mode

```
$ reboot-required \
    --on-shot > /etc/node_exporter/textfile.d/reboot_required.prom
```
