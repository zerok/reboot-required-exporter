package detector

import (
	"context"
	"os"

	"github.com/spf13/afero"
)

type DebianDetector struct {
	fs afero.Fs
}

type DebianConfigurator func(*DebianDetector)

func DebianWithAferoFS(fs afero.Fs) DebianConfigurator {
	return func(d *DebianDetector) {
		d.fs = fs
	}
}

func NewDebianDetector(options ...DebianConfigurator) *DebianDetector {
	d := &DebianDetector{}
	d.fs = afero.NewOsFs()
	for _, o := range options {
		o(d)
	}
	return d
}

func (d *DebianDetector) Detect(ctx context.Context) (bool, error) {
	_, err := d.fs.Stat("/var/run/reboot-required")
	if os.IsNotExist(err) {
		return false, nil
	}
	if err != nil {
		return false, err
	}
	return true, nil
}
