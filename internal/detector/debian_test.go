package detector

import (
	"context"
	"os"
	"testing"

	"github.com/spf13/afero"
	"github.com/stretchr/testify/require"
)

func TestDebianDetector(t *testing.T) {
	fs := afero.NewMemMapFs()
	require.NoError(t, fs.MkdirAll("/var/run", 0755))
	fp, err := fs.OpenFile("/var/run/reboot-required", os.O_CREATE|os.O_TRUNC|os.O_WRONLY, 0644)
	require.NoError(t, err)
	fp.WriteString("")
	require.NoError(t, fp.Close())
	d := NewDebianDetector(DebianWithAferoFS(fs))
	result, err := d.Detect(context.Background())
	require.NoError(t, err)
	require.True(t, result)
}
