package detector

import "context"

type Detector interface {
	Detect(context.Context) (bool, error)
}
