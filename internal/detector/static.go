package detector

import "context"

type StaticDetector struct {
	value bool
}

func (d *StaticDetector) Detect(ctx context.Context) (bool, error) {
	return d.value, nil
}

func NewStaticDetector(value bool) *StaticDetector {
	return &StaticDetector{value: value}
}
