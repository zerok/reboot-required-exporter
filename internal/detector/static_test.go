package detector

import (
	"context"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestStaticDetector(t *testing.T) {
	value, err := NewStaticDetector(true).Detect(context.Background())
	require.NoError(t, err)
	require.True(t, value)
}
