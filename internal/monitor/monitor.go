package monitor

import (
	"context"
	"net/http"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/rs/zerolog"
	"gitlab.com/zerok/reboot-required-exporter/internal/detector"
)

type Configurator func(m *Monitor)

func WithDetector(d detector.Detector) Configurator {
	return func(m *Monitor) {
		m.detector = d
	}
}

type Monitor struct {
	registry             prometheus.Gatherer
	rebootRequiredMetric prometheus.Gauge
	detector             detector.Detector
	handler              http.Handler
}

func NewMonitor(options ...Configurator) *Monitor {
	registry := prometheus.NewRegistry()
	mon := Monitor{}
	mon.registry = registry
	mon.detector = detector.NewStaticDetector(false)
	for _, o := range options {
		o(&mon)
	}
	mon.rebootRequiredMetric = prometheus.NewGauge(prometheus.GaugeOpts{
		Name: "reboot_required",
		Help: "Does the system require a reboot",
	})
	registry.MustRegister(mon.rebootRequiredMetric)
	mon.handler = promhttp.HandlerFor(registry, promhttp.HandlerOpts{})
	return &mon
}

func (m *Monitor) Sample(ctx context.Context) error {
	logger := zerolog.Ctx(ctx)
	logger.Debug().Msg("Sampling.")
	val, err := m.detector.Detect(ctx)
	if err != nil {
		return err
	}
	if val {
		m.rebootRequiredMetric.Set(1)
	} else {
		m.rebootRequiredMetric.Set(0)
	}
	return nil
}

func (m *Monitor) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	m.handler.ServeHTTP(w, r)
}
