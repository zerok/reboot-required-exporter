package monitor

import (
	"context"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/zerok/reboot-required-exporter/internal/detector"
)

func TestMonitor(t *testing.T) {
	mon := NewMonitor(WithDetector(detector.NewStaticDetector(true)))
	require.NoError(t, mon.Sample(context.Background()))
	r := httptest.NewRequest(http.MethodGet, "/", nil)
	w := httptest.NewRecorder()
	mon.ServeHTTP(w, r)
	require.Equal(t, http.StatusOK, w.Code)
	require.Contains(t, w.Body.String(), "reboot_required 1")
}
